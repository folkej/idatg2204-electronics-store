// tailwind.config.js
// @type {import('tailwindcss').Config}
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"], // Add jsx if you are using JSX with React
  theme: {
    extend: {},
  },
  plugins: [],
}
