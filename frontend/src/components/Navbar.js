// src/components/Navbar.js

import React from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

const Navbar = () => {
  const { currentUser, logout } = useAuth();
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      await logout();
      navigate('/login');
    } catch (error) {
      console.error('Error during logout', error);
    }
  };

  const handleHeaderClick = () => {
    // If a user is logged in, navigate to the products page. Otherwise, navigate to the login page.
    if (currentUser) {
      navigate('/products');
    } else {
      navigate('/login');
    }
  };

  return (
    <div className="bg-blue-500 text-white p-4 flex-wrap justify-between items-center">
      <h1 className="font-bold text-xl cursor-pointer" onClick={handleHeaderClick}>Electronics Store</h1>
      <nav className="flex flex-wrap gap-4 gap-2">
        <Link to="/products?category=smartphones" className="hover:text-blue-200">Smartphones</Link>
        <Link to="/products?category=tablets" className="hover:text-blue-200">Tablets</Link>
        <Link to="/products?category=laptops" className="hover:text-blue-200">Laptops</Link>
        <Link to="/products?category=cameras" className="hover:text-blue-200">Cameras</Link>
        <Link to="/products?category=home-appliances" className="hover:text-blue-200">Home Appliances</Link>
        <Link to="/products?category=accessories" className="hover:text-blue-200">Accessories</Link>
        {currentUser && (
          <button
            onClick={handleLogout}
            className="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 rounded transition duration-300 ease-in-out transform hover:-translate-y-1"
          >
            Logout
          </button>
        )}
      </nav>
    </div>
  );
};

export default Navbar;