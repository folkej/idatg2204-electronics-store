// src/components/CartItem.js

import React from 'react';

const CartItem = ({ item, onIncrease, onDecrease }) => {

  return (
    <div className="border p-4 mb-4">
      <h2 className="font-bold">{item.name}</h2>
      <p>Price: ${item.price}</p>
      <p>Quantity: {item.quantity}</p>
      <button className="bg-blue-500 text-white p-2 rounded" onClick={() => onIncrease(item)}>+</button>
      <button className="bg-red-500 text-white p-2 rounded" onClick={() => onDecrease(item)}>-</button>
      <p>Subtotal: ${item.subtotal}</p>
    </div>
  );
};

export default CartItem;