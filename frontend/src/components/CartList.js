// src/components/CartList.js

import React from 'react';
import CartItem from './CartItem';

const CartList = ({ cartItems, onIncrease, onDecrease }) => {
  return (
    <div className="container mx-auto">
      {cartItems.map((item) => (
        <CartItem key={item.cart_item_id} item={item} onIncrease={onIncrease} onDecrease={onDecrease} />
      ))}
    </div>
  );
};

export default CartList;