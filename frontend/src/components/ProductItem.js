// src/components/ProductItem.js

import React from 'react';

const ProductItem = ({ product, onAddToCart }) => {
  return (
    <div className="border p-4 rounded">
      <h2 className="font-bold">{product.Name}</h2>
      <p>{product.Description}</p>
      <p>Price: ${product.Price}</p>
      <p>Stock: {product.StockQuantity}</p>
      <button 
        className="bg-blue-500 text-white p-2 rounded" 
        onClick={() => onAddToCart(product.ProductID)}
      >
        Add to Cart
      </button>
    </div>
  );
};

export default ProductItem;