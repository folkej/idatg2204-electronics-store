// src/components/ProductList.js

import React, { useEffect } from 'react';
import ProductItem from './ProductItem';

const ProductList = ({ products = [], onAddToCart }) => {
  useEffect(() => {
    console.log("Received new products:", products);
  }, [products]);

  return (
    <div className="grid grid-cols-3 gap-4">
      {products.map((product) => (
        <ProductItem key={product.ProductID} product={product} onAddToCart={onAddToCart} />
      ))}
    </div>
  );
};

export default ProductList;