// src/context/AuthContext.js

import React, { createContext, useContext, useState, useEffect } from 'react';
import axios from 'axios';

const AuthContext = createContext();

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState(null);

  // Fetch user details
  useEffect(() => {
    const userJson = localStorage.getItem('user');
    if (userJson) {
      try {
        const user = JSON.parse(userJson);
        setCurrentUser(user);
      } catch (error) {
        console.error("Error parsing user JSON:", error);
      }
    }
  }, []);
  

  const login = async (username, password) => {
    try {
      const response = await axios.post('http://localhost:5002/auth/login', { username, password });
      localStorage.setItem('user', JSON.stringify(response.data.user));
      setCurrentUser(response.data.user);
    } catch (error) {
      console.error('Login error:', error.response?.data?.message || 'Login failed');
      throw new Error(error.response?.data?.message || 'Login failed');
    }
  };

  const logout = async () => {
    try {
      await axios.post('http://localhost:5002/auth/logout');
      localStorage.removeItem('user'); // Clear user from local storage
      setCurrentUser(null); // Clear user from context
    } catch (error) {
      console.error('Logout error:', error);
    }
  };

  const value = {
    currentUser,
    login,
    logout
  };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};