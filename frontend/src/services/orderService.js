// src/services/orderService.js

import axios from 'axios';

const API_URL = 'http://localhost:5002/order/';

export const confirmPayment = async (orderId, paymentMethod) => {
  try {
    const response = await axios.post(`${API_URL}payment/confirm`, {
      order_id: orderId,
      payment_method: paymentMethod
    });
    console.log("Payment confirmed", response);
    return response.data;
  } catch (error) {
    console.error('Error confirming payment:', error.response?.data?.message || 'Could not confirm payment');
    throw error;
  }
};

export const getOrderDetails = async (orderId) => {
  try {
    const response = await axios.get(`${API_URL}order/view/${orderId}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching order details:', error.response?.data?.message || 'Could not fetch order details');
    throw error;
  }
};
