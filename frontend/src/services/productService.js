// frontend/src/services/productService.js

import axios from 'axios';

const API_URL = 'http://localhost:5002/products/products';

axios.defaults.withCredentials = true;

export const getProducts = async (filters = '') => {
  try {
    const response = await axios.get(`${API_URL}${filters}`);
    return response.data;
  } catch (error) {
    console.error('Error fetching products:', error);
    throw error;
  }
};

export const getBrands = async () => {
  try {
    const response = await axios.get(`${API_URL}/brands`);
    return response.data;  // Should return an array of { BrandID, Name }
  } catch (error) {
    console.error('Error fetching brands:', error);
    throw error;
  }
};