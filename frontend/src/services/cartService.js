// src/services/cartService.js

import axios from 'axios';

const API_URL = 'http://localhost:5002/cart/';

axios.defaults.withCredentials = true;

export const addToCart = async (productId, quantity = 1) => {
    try {
        const response = await axios.post(`${API_URL}add_to_cart`, {
            product_id: productId,
            quantity
        });
        console.log("Product added to cart", response.data);
        return response.data;
    } catch (error) {
        console.error('Error adding product to cart:', error.response?.data?.message || 'Could not add product to cart');
        throw error;
    }
};

export const getCartItems = async () => {
  try {
    const response = await axios.get(`${API_URL}view_cart`);
    const { cart_items, total_sum } = response.data; // Extract cart items and total sum from the response
    return { cartItems: cart_items, totalSum: total_sum }; // Return an object containing both cart items and total sum
  } catch (error) {
    console.error('Error fetching cart items:', error);
    throw error;
  }
};


export const adjustQuantity = async (cartItemId, action) => {
  try {
      const response = await axios.post(`${API_URL}adjust_quantity`, {
          cart_item_id: cartItemId,
          action
      });
      console.log("Quantity adjusted", response.data);
      return response.data;
  } catch (error) {
      console.error('Error adjusting quantity:', error.response?.data?.message || 'Could not adjust quantity');
      throw error;
  }
};

export const checkout = async () => {
  try {
    const response = await axios.post(`${API_URL}checkout`);
    console.log("Checkout successful", response.data);
    return response.data;
  } catch (error) {
    console.error('Error checking out:', error.response?.data?.message || 'Could not checkout');
    throw error;
  }
};