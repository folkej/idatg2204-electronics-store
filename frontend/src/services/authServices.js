// src/services/authServices.js

import axios from 'axios';

const API_URL = 'http://localhost:5002/auth/';
axios.defaults.withCredentials = true;

export const login = async (username, password) => {
    try {
      const response = await axios.post(`${API_URL}login`, { username, password });
      console.log("Login successful", response.data);
      return response.data; // Successful login returns a message
    } catch (error) {
      console.error('Login error:', error.response?.data?.message || 'Login failed');
      throw error; // Rethrow the error to be handled in the calling function
    }
  };
  

export const register = async (userDetails) => {
  try {
    const response = await axios.post(`${API_URL}register`, userDetails);
    console.log(response.data);
  } catch (error) {
    console.error('Registration error:', error.response.data);
  }
};
