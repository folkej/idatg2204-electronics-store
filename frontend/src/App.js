// src/App.js

import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import { AuthProvider, useAuth } from './context/AuthContext';
import LoginPage from './pages/LoginPage';
import ProductsPage from './pages/ProductsPage';
import CartPage from './pages/CartPage';
import PaymentPage from './pages/PaymentPage';
import OrderDetails from './pages/OrderDetails';
import './output.css';

function AppWrapper() {
  return (
    <AuthProvider>
      <App />
    </AuthProvider>
  );
}

function App() {
  const { currentUser } = useAuth();

  return (
    <Router>
      <Routes>
        <Route path="/login" element={!currentUser ? <LoginPage /> : <Navigate replace to="/products" />} />
        <Route path="/products" element={<ProductsPage />} />
        <Route path="/cart" element={<CartPage />} />
        <Route path="/payment/:orderId" element={<PaymentPage />} />
        <Route path="/orders/:orderId" element={<OrderDetails />} />
        {/* Redirect any non-matching route to either login or products based on authentication status */}
        <Route path="*" element={!currentUser ? <Navigate replace to="/login" /> : <Navigate replace to="/products" />} />
      </Routes>
    </Router>
  );
}

export default AppWrapper;
