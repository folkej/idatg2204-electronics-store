// src/pages/PaymentPage.js

import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { confirmPayment } from '../services/orderService';

const PaymentPage = () => {
  const [paymentMethod, setPaymentMethod] = useState('Credit Card');
  const { orderId } = useParams(); // Retrieve orderId from URL parameters
  const navigate = useNavigate();

  const handlePayment = async (e) => {
    e.preventDefault(); // Prevent the default form submission behavior

    try {
      // Use the confirmPayment service function
      await confirmPayment(orderId, paymentMethod);

      // Navigate to the order details page upon successful payment
      navigate(`/orders/${orderId}`); 
    } catch (error) {
      console.error('Error processing payment:', error);
    }
  };

  return (
    <div className="container mx-auto">
      <h1 className="text-2xl font-bold mb-4">Payment</h1>
      <form onSubmit={handlePayment}>
        <div className="mb-4">
          <label className="block text-sm font-medium text-gray-700 mb-2">Payment Method:</label>
          <div className="flex flex-col">
            <label><input type="radio" value="Credit Card" checked={paymentMethod === 'Credit Card'} onChange={(e) => setPaymentMethod(e.target.value)} /> Credit Card</label>
            <label><input type="radio" value="Invoice" checked={paymentMethod === 'Invoice'} onChange={(e) => setPaymentMethod(e.target.value)} /> Invoice</label>
            <label><input type="radio" value="Debit Card" checked={paymentMethod === 'Debit Card'} onChange={(e) => setPaymentMethod(e.target.value)} /> Debit Card</label>
          </div>
        </div>
        <button type="submit" className="bg-green-500 text-white px-4 py-2 rounded">Confirm Payment</button>
      </form>
    </div>
  );
};

export default PaymentPage;