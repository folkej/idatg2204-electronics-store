// src/pages/LoginPage.js

import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import LoginForm from '../components/LoginForm';
import RegisterForm from '../components/RegisterForm';
import * as authService from '../services/authServices';

function LoginPage() {
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState('');

  const handleLogin = async (username, password) => {
    try {
      await authService.login(username, password);
      navigate('/products'); // Redirect to /products upon successful login
    } catch (error) {
      const message = error.response?.data.message || 'Login failed';
      setErrorMessage(message);
      console.error(message);
    }
  };

  const handleRegister = async (userDetails) => {
    try {
      const response = await authService.register(userDetails);
      console.log(response); // Placeholder for handling the response
      await handleLogin(userDetails.username, userDetails.password);
    } catch (error) {
      const message = error.response?.data.message || 'Registration failed';
      setErrorMessage(message);
      console.error(message);
    }
  };

  return (
    <div className="min-h-screen bg-gray-100 flex flex-col justify-center items-center px-4">
      <h1 className="text-2xl md:text-3xl lg:text-4xl font-bold text-center mb-4 text-gray-800">
        Welcome to the exclusive Electro-Store. To shop, you must have a user.
      </h1>
      {errorMessage && <p className="text-red-500">{errorMessage}</p>}
      <div className="w-full max-w-md bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <LoginForm onLogin={handleLogin} />
      </div>
      <span className="text-gray-500 my-4">Or</span>
      <div className="w-full max-w-md bg-white shadow-md rounded px-8 pt-6 pb-8">
        <RegisterForm onRegister={handleRegister} />
      </div>
    </div>
  );
}

export default LoginPage;