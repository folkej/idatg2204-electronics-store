// src/pages/CartPage.js

import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom'; // Import useNavigate
import { getCartItems, adjustQuantity, checkout } from '../services/cartService';
import CartList from '../components/CartList';

const CartPage = () => {
  const [cartItems, setCartItems] = useState([]);
  const [totalSum, setTotalSum] = useState(0);
  const navigate = useNavigate(); // Initialize useNavigate

  const fetchCartItems = async () => {
    try {
      const response = await getCartItems();
      const { cartItems, totalSum } = response;
      setCartItems(cartItems);
      setTotalSum(totalSum);

    } catch (error) {
      console.error('Error fetching cart items:', error);
    }
  };

  useEffect(() => {

    fetchCartItems();
  }, []);

  const handleIncreaseQuantity = async (item) => {
    try {
      await adjustQuantity(item.cart_item_id, 'increase');
      fetchCartItems(); // fetch updated cart items
    } catch (error) {
      console.error('Error increasing quantity:', error);
    }
  };

  const handleDecreaseQuantity = async (item) => {
    try {
      await adjustQuantity(item.cart_item_id, 'decrease');
      fetchCartItems(); 
    } catch (error) {
      console.error('Error decreasing quantity:', error);
    }
  };

  const handleCheckout = async () => {
    try {
      const response = await checkout();
      console.log("Checkout successful", response);
      // Redirect to payment page
      navigate(`/payment/${response.order_id}`);
    } catch (error) {
      console.error('Error checking out:', error);
    }
  };

  return (
    <div className="container mx-auto">
      <h1 className="text-2xl font-bold mb-4">Shopping Cart</h1>
      {cartItems.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        <div>
          <CartList cartItems={cartItems} onIncrease={handleIncreaseQuantity} onDecrease={handleDecreaseQuantity} />
          <p>Total: ${totalSum}</p> {/* Display total sum */}
          <button onClick={() => navigate('/products')} className="bg-gray-500 text-white px-4 py-2 rounded">Back to Store</button>
          <button onClick={handleCheckout} className="bg-green-500 text-white p-2 rounded">Proceed to Checkout</button>
        </div>
      )}
    </div>
  );
};

export default CartPage;