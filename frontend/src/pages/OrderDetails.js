// /src/pages/OrderDetails.js

import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { getOrderDetails } from '../services/orderService';

const OrderDetails = () => {
  const { orderId } = useParams();
  const [orderDetails, setOrderDetails] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchOrderDetails = async () => {
      try {
        const response = await getOrderDetails(orderId);
        setOrderDetails(response);
      } catch (error) {
        console.error('Error fetching order details:', error);
      }
    };

    fetchOrderDetails();
  }, [orderId]);

  if (!orderDetails) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-8">Order Details</h1>

      <div className="mb-8 p-4 shadow-lg rounded-lg">
        <h2 className="text-xl font-semibold mb-4">Order Information</h2>
        <p><strong>Order ID:</strong> {orderDetails.OrderID}</p>
        <p><strong>Order Date:</strong> {orderDetails.OrderDate}</p>
        <p><strong>Total Amount:</strong> ${orderDetails.TotalAmount}</p>
        <p><strong>Status:</strong> {orderDetails.Status}</p>
      </div>

      <div className="mb-8 p-4 shadow-lg rounded-lg">
        <h2 className="text-xl font-semibold mb-4">Contact Information</h2>
        <ul className="list-disc pl-5">
          <li><strong>First Name:</strong> {orderDetails.UserDetails.FirstName}</li>
          <li><strong>Last Name:</strong> {orderDetails.UserDetails.LastName}</li>
          <li><strong>Address:</strong> {orderDetails.UserDetails.Address}</li>
        </ul>
      </div>

      <div className="mb-8 p-4 shadow-lg rounded-lg">
        <h2 className="text-xl font-semibold mb-4">Order Items</h2>
        <ul className="list-disc pl-5">
          {orderDetails.Items.map((item, index) => (
            <li key={index} className="mb-2">
              <p><strong>Product Name:</strong> {item.ProductName}</p>
              <p><strong>Quantity:</strong> {item.Quantity}</p>
              <p><strong>Subtotal:</strong> ${item.Subtotal}</p>
            </li>
          ))}
        </ul>
      </div>

      <button
        onClick={() => navigate('/products')}
        className="fixed bottom-8 right-8 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full shadow-lg"
      >
        Back to Store
      </button>
    </div>
  );
};

export default OrderDetails;