// src/pages/ProductsPage.js

import React, { useEffect, useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import ProductList from '../components/ProductList';
import Navbar from '../components/Navbar';
import { getProducts, getBrands } from '../services/productService';
import { addToCart } from '../services/cartService';

const ProductsPage = () => {
  const [products, setProducts] = useState([]);
  const [brands, setBrands] = useState([]); // State to hold brands
  const [selectedBrands, setSelectedBrands] = useState([]); // State to hold selected brands
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();
  const location = useLocation();

  const [sort, setSort] = useState('name_asc');
  const [inStock, setInStock] = useState(false);

  useEffect(() => {
    const loadBrands = async () => {
      try {
        const brandsData = await getBrands();
        setBrands(brandsData); // Corrected to use the correct variable
      } catch (error) {
        console.error('Failed to fetch brands', error);
      }
    };

    loadBrands();
  }, []);

  useEffect(() => {
    const fetchProducts = async () => {
      const queryParams = new URLSearchParams(location.search);
      if (sort) queryParams.set('sort', sort);
      if (inStock) queryParams.set('in_stock', inStock);
      if (selectedBrands.length > 0) queryParams.set('brands', selectedBrands.join(','));

      try {
        const response = await getProducts(`?${queryParams.toString()}`);
        setProducts(response);
        setLoading(false);
      } catch (error) {
        console.error('Failed to fetch products', error);
        setLoading(false);
      }
    };

    fetchProducts();
  }, [location.search, sort, inStock, selectedBrands]);

  const handleAddToCart = async (productId) => {
    try {
      await addToCart(productId);
      console.log("Product added to cart successfully!");
    } catch (error) {
      console.error("Failed to add product to cart:", error);
    }
  };

  const handleBrandChange = (event) => {
    const value = Array.from(event.target.selectedOptions, option => option.value);
    setSelectedBrands(value);
  };

  return (
    <div className="p-4">
      <Navbar />
      <div className="flex flex-col md:flex-row items-center justify-between mb-4">
        <select onChange={(e) => setSort(e.target.value)} className="mb-4 md:mb-0 block w-full md:w-auto bg-white border border-gray-300 text-gray-700 py-2 px-4 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
          <option value="name_asc">Name Ascending</option>
          <option value="price_asc">Price Ascending</option>
          <option value="price_desc">Price Descending</option>
        </select>

        <label className="flex items-center space-x-2">
          <input type="checkbox" checked={inStock} onChange={(e) => setInStock(e.target.checked)} className="form-checkbox h-5 w-5"/>
          <span className="text-gray-700">In Stock Only</span>
        </label>

        <select multiple={true} value={selectedBrands} onChange={handleBrandChange} className="form-multiselect block w-full mt-1">
          {brands.map((brand) => (
            <option key={brand.BrandID} value={brand.Name}>
              {brand.Name}
            </option>
          ))}
        </select>
      </div>

      {loading ? (
        <div>Loading...</div>
      ) : (
        <ProductList products={products} onAddToCart={handleAddToCart} />
      )}

      <button className="fixed bottom-4 right-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded shadow-lg transform hover:scale-105 transition-transform duration-200 ease-in-out" onClick={() => navigate('/cart')}>
        View Cart
      </button>
    </div>
  );
};

export default ProductsPage;