# order.py

from flask import Blueprint, jsonify, request
from models import db, UserDetails, Order, OrderItem, Payment
from flask_login import login_required, current_user
from datetime import datetime

order_blueprint = Blueprint('order', __name__)

@order_blueprint.route('/payment/confirm', methods=['POST'])
@login_required
def confirm_payment():
    print(request)
    print(request.json)
    order_id = request.json.get('order_id')
    payment_method = request.json.get('payment_method')
    
    order = Order.query.get(order_id)
    if not order or order.UserID != int(current_user.get_id()):
        return jsonify({'message': 'Order not found'}), 404

    # Record payment
    payment = Payment(OrderID=order_id, PaymentMethod=payment_method, Amount=order.TotalAmount, PaymentDate=datetime.utcnow(), Status="Processing")
    db.session.add(payment)

    # Update order status
    order.Status = "Processing"
    db.session.commit()

    return jsonify({'message': 'Payment confirmed', 'order_id': order_id}), 200

@order_blueprint.route('/order/view/<int:order_id>', methods=['GET'])
@login_required
def view_order(order_id):
    order = Order.query.get(order_id)
    if not order or order.UserID != int(current_user.get_id()):
        return jsonify({'message': 'Order not found'}), 404
    
    user_details = UserDetails.query.filter_by(UserID=order.UserID).first()

    order_items = OrderItem.query.filter_by(OrderID=order_id).all()
    order_details = {
        'OrderID': order.OrderID,
        'OrderDate': order.OrderDate.strftime('%Y-%m-%d'),
        'TotalAmount': str(order.TotalAmount),
        'Status': order.Status,
        'Items': [{'ProductName': item.product.Name, 'Quantity': item.Quantity, 'Subtotal': str(item.Subtotal)} for item in order_items],
        'UserDetails': {
            'FirstName' : user_details.FirstName,
            'LastName' : user_details.LastName,
            'Address' : user_details.Address
        }
    }

    return jsonify(order_details), 200