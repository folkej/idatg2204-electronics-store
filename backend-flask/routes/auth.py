#auth.py

from flask import Blueprint, request, jsonify
from flask_login import login_user
from models import db, UserCredentials, UserDetails
from extensions import db, bcrypt
from flask_login import logout_user

auth_blueprint = Blueprint('auth', __name__)

@auth_blueprint.route('/register', methods=['POST'])
def register():
    username = request.json.get('username')
    email = request.json.get('email')
    plain_password = request.json.get('password')
    first_name = request.json.get('firstName')
    last_name = request.json.get('lastName')
    address = request.json.get('address')

    # Check if user already exists
    existing_user = UserCredentials.query.filter_by(Username=username).first()
    if existing_user:
        return jsonify({'message': 'Username already taken'}), 400

    # Hash password
    hashed_password = bcrypt.generate_password_hash(plain_password).decode('utf-8')

    # Create new user
    new_user = UserCredentials(Username=username, Email=email, Password=hashed_password)
    db.session.add(new_user)
    db.session.flush() # Assigns an ID to the new_user without commiting yet

    # Create new user details
    new_user_details = UserDetails(UserID=new_user.UserID, FirstName=first_name, LastName=last_name, Address=address)
    db.session.add(new_user_details)
    db.session.commit()

    return jsonify({'message': 'User registered successfully'}), 201

@auth_blueprint.route('/login', methods=['POST'])
def login():
    username = request.json.get('username')
    password = request.json.get('password')

    user = UserCredentials.query.filter_by(Username=username).first()

    if user and bcrypt.check_password_hash(user.Password, password):
        login_user(user)    # Log in the user with Flask-Login
        return jsonify({'user': {'username': user.Username, 'email': user.Email}}), 200
    else:
        return jsonify({'message': 'Invalid username or password'}), 401
    
@auth_blueprint.route('/logout', methods=['POST'])
def logout():
    logout_user()
    return jsonify({'message': 'Logout successful'}), 200