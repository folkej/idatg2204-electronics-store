# cart.py

from flask import Blueprint, jsonify, request
from models import db, Product, Order, OrderItem, Cart, CartItem
from flask_login import login_required, current_user
from datetime import datetime

cart_blueprint = Blueprint('cart', __name__)

@cart_blueprint.route('/add_to_cart', methods=['POST'])
@login_required
def add_to_cart():
    user_id = current_user.get_id()
    product_id = request.json.get('product_id')
    quantity = request.json.get('quantity', 1)  # Defaults to 1

    # Check if product exists
    product = Product.query.get(product_id)
    if not product:
        return jsonify({'message': 'Product not found'}), 404
    
    # Check if user already has a cart, otherwise create one
    cart = Cart.query.filter_by(UserID=user_id).first() # .first - simple way to check if not none. 
    if not cart:
        cart = Cart(UserID=user_id)
        db.session.add(cart)
        db.session.commit()

    # Check if the product is already in the cart
    cart_item = CartItem.query.filter_by(CartID=cart.CartID, ProductID=product_id).first()
    if cart_item:
        # If already in the cart, increase the quantity
        cart_item.Quantity += quantity
    else:
        # If not in the cart, add new item to cart
        cart_item = CartItem(CartID=cart.CartID, ProductID=product_id, Quantity=quantity)
        db.session.add(cart_item)

    db.session.commit()

    return jsonify({'message': 'Product added to cart'}), 200

@cart_blueprint.route('/view_cart', methods=['GET'])
@login_required
def view_cart():
    user_id = current_user.get_id()
    cart = Cart.query.filter_by(UserID=user_id).first()

    if not cart:
        return jsonify({'message': 'No cart found'}), 404

    cart_items = CartItem.query.filter_by(CartID=cart.CartID).all()
    cart_view = []
    total_sum = 0

    for item in cart_items:
        product = Product.query.get(item.ProductID)
        subtotal = item.Quantity * product.Price
        total_sum += subtotal 
        cart_view.append({
            'cart_item_id': item.CartItemID,
            'product_id': product.ProductID,
            'name': product.Name,
            'price': str(product.Price),
            'quantity': item.Quantity,
            'subtotal': str(subtotal)
        })

    # Return the cart items along with the total sum of the cart
    response = {
        'cart_items': cart_view,
        'total_sum': str(total_sum),  # Convert total sum to string for JSON serialization
    }
    return jsonify(response), 200

@cart_blueprint.route('/adjust_quantity', methods=['POST'])
@login_required
def adjust_quantity():
    print(request)
    print(request.json)
    cart_item_id = request.json.get('cart_item_id')
    action = request.json.get('action')  # "increase" or "decrease"
    user_id = current_user.get_id()

    # Retrieve the user's cart. If no cart is found, return an error
    cart = Cart.query.filter_by(UserID=user_id).first()
    if not cart:
        return jsonify({'message': 'Cart not found'}), 404

    # Retrieve the cart item and verify it belongs to the user's cart
    cart_item = CartItem.query.get(cart_item_id)
    if not cart_item:
        return jsonify({'message': 'Cart item not found'}), 404

    if action == 'increase':
        cart_item.Quantity += 1
    elif action == 'decrease':
        cart_item.Quantity -= 1
        if cart_item.Quantity < 1:
            db.session.delete(cart_item)
    else:
        return jsonify({'message': 'Invalid action'}), 400

    db.session.commit()
    return jsonify({'message': 'Cart updated'}), 200

@cart_blueprint.route('/checkout', methods=['POST'])
@login_required
def checkout():
    user_id = current_user.get_id()
    cart = Cart.query.filter_by(UserID=user_id).first()
    if not cart or len(cart.cart_items) == 0:
        return jsonify({'message': 'Cart is empty'}), 400

    # Retrieve cart items in the cart
    cart_items = CartItem.query.filter_by(CartID=cart.CartID).all()
    if len(cart_items) == 0:
        return jsonify({'message': 'Cart is empty'}), 400

    # Calculate total amount of cart
    total_amount = sum(item.Quantity * item.product.Price for item in cart_items)

    # Create a new order
    order = Order(UserID=user_id, OrderDate=datetime.utcnow(), Status="Pending", TotalAmount = str(total_amount))
    db.session.add(order)
    db.session.flush()  # To get the order ID before committing

    # Convert cart items to order items
    for item in cart.cart_items:
        order_item = OrderItem(OrderID=order.OrderID, ProductID=item.ProductID, Quantity=item.Quantity, Subtotal=item.Quantity * item.product.Price)
        db.session.add(order_item)
        db.session.delete(item)  # Remove item from cart

    db.session.commit()

    return jsonify({'message': 'Checkout successful', 'order_id': order.OrderID}), 200