# products.py
# Define the routes that handle API requests.

"""
Some notes: 
- SQLAlchemys query-object represents a database query. One can use its methods like filter(), order_by(), and join() which corresponds to SQL-commands. filter() correspond to the WHERE clauses in the query. One could for example write Product.query.filter(Product.StockQuantity > 0), which would generate a query to the Product table (or the Product object which we have defined in models), where the column StockQuantity is greater than zero. 
- SQLAlchemys request-object contains all information sent by the client to the web server. request.args is a multidict containing all the query parameters from the URL. For example, if the URL is 'http://localhost:5002?in_stock=true', request.args will have a key named in_stock with its value set to true. get is used to retrieve the value for the in_stock key. One can set a default value in case there is no such key in the query parameters, and it is good practice to lowercase, in case of 'True', or 'TRUE'. Thus we get the line: in_stock = request.args.get('in_stock', 'false').lower() == 'true'
- The use of backrefs greatly reduce the footprint of the code by avoiding long join statements. For more on backrefs, consult models.py. 
- Decimals are converted to strings before JSON serialization to avoid rounding errors. The recipient of the JSON converts the string back to a number. 
"""

from flask import Blueprint, jsonify, request #redirect, url_for
# from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user    # For session management
from models import Product, Brand, Category, ProductCategory

product_blueprint = Blueprint('products', __name__)

@product_blueprint.route('/products', methods=['GET'])
def get_products():
    # Start with querying the Product model
    print("Query Parameters Received:", request.args)
    query = Product.query

    # Modifying the query before actually querying the Product Table:
    # Checks if there is a query parameter named sort, else defaults to sort=name_asc
    sort_order = request.args.get('sort', 'name_asc')

    # Sorting: either by price ascending or descending, defaults to alphabetical
    if sort_order == 'price_asc':
        query = query.order_by(Product.Price.asc())
    elif sort_order == 'price_desc':
        query = query.order_by(Product.Price.desc())
    else:
        query = query.order_by(Product.Name.asc())

    # Filtering by in-stock status
    in_stock = request.args.get('in_stock', 'false').lower() == 'true'
    if in_stock:
        query = query.filter(Product.StockQuantity > 0)

    # Filtering by brands
    brands = request.args.get('brands')
    if brands:
        brand_list = brands.split(',')  # creates a list of brands
        query = query.join(Product.brand).filter(Brand.Name.in_(brand_list))    # in_ is SQLs IN

    # Filtering by category
    category_name = request.args.get('category')
    if category_name:
        #category_name = category_name.replace('-', ' ') # For "Home Appliances"
    # Joining the junction table ProductCategory and the Category table through backrefs
        query = query.join(ProductCategory, Product.ProductID == ProductCategory.ProductID) \
        .join(Category, ProductCategory.CategoryID == Category.CategoryID) \
        .filter(Category.Name == category_name)

    # After sorting and filtering applied to the query object, execute query:
    products = query.all()

    # Create a JSON-list through looping through the products
    # Brand is accessed directly (without need for JOIN) through backreferencing:
    # There is a backref to Brand in the Product model which we can access through product.brand
    products_json = [{
        'ProductID': product.ProductID,
        'Name': product.Name,
        'Description': product.Description,
        'Price': str(product.Price),  # Convert Decimal to string for JSON serialization
        'StockQuantity': product.StockQuantity,
        'Brand': product.brand.Name if product.brand else None
    } for product in products]

    # Serializes the list of dictionaries into JSON-formatted string, also sets Content-Type header. 
    return jsonify(products_json)

@product_blueprint.route('products/brands', methods=['GET'])
def get_brands():
    brands = Brand.query.all()  # Fetch all brands from the database
    brands_json = [{'BrandID': brand.BrandID, 'Name': brand.Name} for brand in brands]  # Convert to JSON format
    
    return jsonify(brands_json)