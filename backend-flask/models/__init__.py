# __init__.py

from flask_sqlalchemy import SQLAlchemy
from extensions import db

# Import models to ensure they're known to SQLAlchemy
from .models import UserCredentials, UserDetails, Product, Brand, Category, Order, OrderItem, Cart, CartItem, ProductCategory, Payment
