# models.py
# Defines the SQLAlchemy models that represent the database schema

"""
- backref in SQLAlchemy is a way to add a reverse relationship (backreference) so as to access related objects easily from both sides of the relationship. It is a keyword argument passed to db.relationship(), which SQLAlchemy uses to automatically create a new property on the backreferenced class. Using this newly created property, we can access the list of related objects from the other side. For example, in the one-to-many relationship between UserCredentials and Orders, by having a backreference to UserCredentials in Order, we might easily access all orders for a user through user.orders. Where the backrefs are placed, depends on the type of relationship, one-to-one (UserCredentials and UserDetails), one-to-many, and many-to-many (Product and Category).
- uselist=False is used to specify that we access the single related item directly, not as a list. For example, to access payment from order, then we can simply write order.payment, instead of order.payment[0].
"""

from flask_login import UserMixin   # For UserAuthentication
from extensions import db

class Brand(db.Model):
    __tablename__ = 'Brand'
    BrandID = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(255), unique=True, nullable=False)
    Description = db.Column(db.Text)

class Category(db.Model):
    __tablename__ = 'Category'
    CategoryID = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(255), unique=True, nullable=False)
    Description = db.Column(db.Text)

class UserCredentials(db.Model, UserMixin):
    __tablename__ = 'UserCredentials'
    UserID = db.Column(db.Integer, primary_key=True)
    Username = db.Column(db.String(255), unique=True, nullable=False)
    Password = db.Column(db.String(60), nullable=False)
    Email = db.Column(db.String(255), unique=True, nullable=False)

    def get_id(self):
        """
        Returns the user identifier as a unicode string.
        Flask-Login uses this to handle user sessions.
        """
        return str(self.UserID)

class UserDetails(db.Model):
    __tablename__ = 'UserDetails'
    UserID = db.Column(db.Integer, db.ForeignKey('UserCredentials.UserID'), primary_key=True)
    FirstName = db.Column(db.String(255), nullable=False)
    LastName = db.Column(db.String(255), nullable=False)
    Address = db.Column(db.Text, nullable=False)
    user_credentials = db.relationship('UserCredentials', backref=db.backref('user_details', uselist=False))

class Product(db.Model):
    __tablename__ = 'Product'
    ProductID = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(255), nullable=False)
    Description = db.Column(db.Text)
    Price = db.Column(db.Numeric(10, 2), nullable=False)
    StockQuantity = db.Column(db.Integer, nullable=False)
    BrandID = db.Column(db.Integer, db.ForeignKey('Brand.BrandID'))
    brand = db.relationship('Brand', backref='products')

class Order(db.Model):
    __tablename__ = 'Order'
    OrderID = db.Column(db.Integer, primary_key=True)
    UserID = db.Column(db.Integer, db.ForeignKey('UserCredentials.UserID'))
    OrderDate = db.Column(db.Date, nullable=False)
    TotalAmount = db.Column(db.Numeric(10, 2), nullable=False)
    Status = db.Column(db.String(255), nullable=False)
    user = db.relationship('UserCredentials', backref='orders')

class Payment(db.Model):
    __tablename__ = 'Payment'
    OrderID = db.Column(db.Integer, db.ForeignKey('Order.OrderID'), primary_key=True)
    PaymentMethod = db.Column(db.String(255), nullable=False)
    Amount = db.Column(db.Numeric(10, 2), nullable=False)
    PaymentDate = db.Column(db.Date, nullable=False)
    Status = db.Column(db.String(255), nullable=False)
    order = db.relationship('Order', backref=db.backref('payment', uselist=False))

class OrderItem(db.Model):
    __tablename__ = 'OrderItem'
    OrderItemID = db.Column(db.Integer, primary_key=True)
    OrderID = db.Column(db.Integer, db.ForeignKey('Order.OrderID'), nullable=False)
    ProductID = db.Column(db.Integer, db.ForeignKey('Product.ProductID'), nullable=False)
    Quantity = db.Column(db.Integer, nullable=False)
    Subtotal = db.Column(db.Numeric(10, 2), nullable=False)
    order = db.relationship('Order', backref='order_items')
    product = db.relationship('Product', backref='order_items')

class Cart(db.Model):
    __tablename__ = 'Cart'
    CartID = db.Column(db.Integer, primary_key=True)
    UserID = db.Column(db.Integer, db.ForeignKey('UserCredentials.UserID'), nullable=False)
    user = db.relationship('UserCredentials', backref='cart')

class CartItem(db.Model):
    __tablename__ = 'CartItem'
    CartItemID = db.Column(db.Integer, primary_key=True)
    CartID = db.Column(db.Integer, db.ForeignKey('Cart.CartID'), nullable=False)
    ProductID = db.Column(db.Integer, db.ForeignKey('Product.ProductID'))
    Quantity = db.Column(db.Integer, nullable=False)
    cart = db.relationship('Cart', backref='cart_items')
    product = db.relationship('Product', backref='cart_items')

class ProductCategory(db.Model):
    __tablename__ = 'ProductCategory'
    ProductID = db.Column(db.Integer, db.ForeignKey('Product.ProductID'), primary_key=True)
    CategoryID = db.Column(db.Integer, db.ForeignKey('Category.CategoryID'), primary_key=True)
    product = db.relationship('Product', backref='categories')
    category = db.relationship('Category', backref='products')
