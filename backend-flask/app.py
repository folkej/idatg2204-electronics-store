# app.py
# Main file where Flask application is defined and run
# We initialize the app here, configure the database and register Blueprints

from flask import Flask, redirect, url_for
from flask_login import current_user
from extensions import db, bcrypt, login_manager
from models import db
import secrets
from flask_cors import CORS

def create_app():
    app = Flask(__name__)

    app.secret_key = secrets.token_hex(16)  # Generate a random 32-character hexadecimal string
    # Configure CORS to allow credentials from the React app's origin
    CORS(app, supports_credentials=True, origins=["http://localhost:3000"])

    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@db:3306/Electronics-Store'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    # Initialize Flask extensions
    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)

    # Set the login view
    login_manager.login_view = 'auth.login'

    # User loader
    @login_manager.user_loader
    def load_user(user_id):
        from models import UserCredentials
        return UserCredentials.query.get(int(user_id))

    # Register Blueprints
    from routes.auth import auth_blueprint
    from routes.products import product_blueprint
    from routes.cart import cart_blueprint
    from routes.order import order_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')
    app.register_blueprint(product_blueprint, url_prefix='/products')
    app.register_blueprint(cart_blueprint, url_prefix='/cart')
    app.register_blueprint(order_blueprint, url_prefix='/order')

    # Defining index route
    @app.route('/')
    def home():
        if current_user.is_authenticated:
            return redirect(url_for('products.get_products'))
        else:
            return redirect(url_for('auth.login'))

    return app

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', debug=True, port=5000)