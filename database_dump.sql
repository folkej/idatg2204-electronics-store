-- MySQL dump 10.13  Distrib 5.7.24, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: Electronics-Store
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Brand`
--

DROP TABLE IF EXISTS `Brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Brand` (
  `BrandID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` text DEFAULT NULL,
  PRIMARY KEY (`BrandID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=1007 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Brand`
--

LOCK TABLES `Brand` WRITE;
/*!40000 ALTER TABLE `Brand` DISABLE KEYS */;
INSERT INTO `Brand` VALUES (1001,'Samsung','South Korean multinational electronics company.'),(1002,'Google','American multinational technology company.'),(1003,'Apple','American multinational technology company.'),(1004,'Sony','Japanese multinational conglomerate.'),(1005,'LG','South Korean multinational conglomerate.'),(1006,'Dell','American multinational computer technology company.');
/*!40000 ALTER TABLE `Brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cart`
--

DROP TABLE IF EXISTS `Cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cart` (
  `CartID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`CartID`),
  KEY `idx_cart_user_id` (`UserID`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `UserCredentials` (`UserID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9008 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cart`
--

LOCK TABLES `Cart` WRITE;
/*!40000 ALTER TABLE `Cart` DISABLE KEYS */;
INSERT INTO `Cart` VALUES (9001,1),(9002,2),(9003,5),(9005,7),(9007,10),(9004,11),(9006,12);
/*!40000 ALTER TABLE `Cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CartItem`
--

DROP TABLE IF EXISTS `CartItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CartItem` (
  `CartItemID` int(11) NOT NULL AUTO_INCREMENT,
  `CartID` int(11) NOT NULL,
  `ProductID` int(11) DEFAULT NULL,
  `Quantity` int(11) NOT NULL,
  PRIMARY KEY (`CartItemID`),
  KEY `CartID` (`CartID`),
  KEY `idx_cartitem_product_id` (`ProductID`),
  CONSTRAINT `cartitem_ibfk_1` FOREIGN KEY (`CartID`) REFERENCES `Cart` (`CartID`) ON UPDATE CASCADE,
  CONSTRAINT `cartitem_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CartItem`
--

LOCK TABLES `CartItem` WRITE;
/*!40000 ALTER TABLE `CartItem` DISABLE KEYS */;
INSERT INTO `CartItem` VALUES (1,9001,5001,3),(2,9001,5003,1),(3,9001,5008,1),(4,9002,5002,2),(5,9002,5004,1),(6,9002,5006,1),(19,9004,5010,3),(20,9004,5009,1),(21,9004,5012,1),(22,9004,5001,1);
/*!40000 ALTER TABLE `CartItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` text DEFAULT NULL,
  PRIMARY KEY (`CategoryID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=2007 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (2001,'smartphones','Handheld mobile devices with touchscreens.'),(2002,'tablets','Portable touchscreen devices larger than smartphones.'),(2003,'laptops','Portable personal computers.'),(2004,'cameras','Devices for capturing photographs.'),(2005,'home-appliances','Electrical machines that help in household functions.'),(2006,'accessories','Additional equipment for convenience, attractiveness, or safety.');
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order`
--

DROP TABLE IF EXISTS `Order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Order` (
  `OrderID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `OrderDate` date NOT NULL,
  `TotalAmount` decimal(10,2) NOT NULL,
  `Status` varchar(255) NOT NULL,
  PRIMARY KEY (`OrderID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `UserCredentials` (`UserID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1025 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order`
--

LOCK TABLES `Order` WRITE;
/*!40000 ALTER TABLE `Order` DISABLE KEYS */;
INSERT INTO `Order` VALUES (1001,1,'2024-01-15',299.99,'Shipped'),(1002,2,'2024-01-16',89.99,'Ready to ship'),(1003,3,'2024-02-01',3249.98,'Processing'),(1004,4,'2024-02-05',249.99,'Shipped'),(1005,5,'2024-03-30',5499.98,'Pending'),(1006,5,'2024-03-30',7200.00,'Pending'),(1007,5,'2024-03-30',7200.00,'Pending'),(1008,5,'2024-03-30',7200.00,'Processing'),(1009,5,'2024-03-30',7200.00,'Processing'),(1010,5,'2024-03-30',21299.97,'Processing'),(1011,7,'2024-04-02',18959.90,'Pending'),(1012,7,'2024-04-02',13549.98,'Pending'),(1013,7,'2024-04-02',499.98,'Pending'),(1014,7,'2024-04-03',1249.96,'Pending'),(1015,7,'2024-04-03',4549.97,'Processing'),(1016,7,'2024-04-03',2949.95,'Processing'),(1017,7,'2024-04-03',5429.94,'Processing'),(1018,7,'2024-04-03',5699.94,'Processing'),(1019,12,'2024-04-05',3699.97,'Processing'),(1020,7,'2024-04-07',4729.97,'Processing'),(1021,7,'2024-04-11',8160.00,'Processing'),(1022,7,'2024-04-12',3749.96,'Processing'),(1023,7,'2024-04-12',980.00,'Processing'),(1024,10,'2024-04-14',4939.98,'Processing');
/*!40000 ALTER TABLE `Order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderItem`
--

DROP TABLE IF EXISTS `OrderItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderItem` (
  `OrderItemID` int(11) NOT NULL AUTO_INCREMENT,
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Subtotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`OrderItemID`),
  KEY `OrderID` (`OrderID`),
  KEY `ProductID` (`ProductID`),
  CONSTRAINT `orderitem_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `Order` (`OrderID`) ON UPDATE CASCADE,
  CONSTRAINT `orderitem_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderItem`
--

LOCK TABLES `OrderItem` WRITE;
/*!40000 ALTER TABLE `OrderItem` DISABLE KEYS */;
INSERT INTO `OrderItem` VALUES (1,1001,5001,1,299.99),(2,1002,5002,1,89.99),(3,1003,5005,2,2400.00),(4,1003,5007,1,2500.00),(5,1004,5010,1,249.99),(6,1005,5004,1,899.99),(7,1005,5003,1,999.99),(8,1005,5005,3,3600.00),(9,1006,5005,6,7200.00),(10,1007,5005,6,7200.00),(11,1008,5005,6,7200.00),(12,1009,5005,6,7200.00),(13,1010,5005,9,10800.00),(14,1010,5007,3,7500.00),(15,1010,5003,3,2999.97),(16,1011,5013,3,1499.97),(17,1011,5005,2,2400.00),(18,1011,5009,1,999.99),(19,1011,5012,2,99.98),(20,1011,5008,2,1960.00),(21,1011,5010,2,499.98),(22,1011,5003,1,999.99),(23,1011,5007,4,10000.00),(24,1011,5002,1,499.99),(25,1012,5007,5,12500.00),(26,1012,5012,1,49.99),(27,1012,5009,1,999.99),(28,1013,5010,2,499.98),(29,1014,5010,3,749.97),(30,1014,5013,1,499.99),(31,1015,5010,1,249.99),(32,1015,5007,1,2500.00),(33,1015,5001,1,799.99),(34,1015,5009,1,999.99),(35,1016,5010,3,749.97),(36,1016,5013,1,499.99),(37,1016,5005,1,1200.00),(38,1016,5002,1,499.99),(39,1017,5013,4,1999.96),(40,1017,5010,1,249.99),(41,1017,5005,1,1200.00),(42,1017,5009,1,999.99),(43,1017,5008,1,980.00),(44,1018,5001,1,799.99),(45,1018,5003,4,3999.96),(46,1018,5004,1,899.99),(47,1019,5013,1,499.99),(48,1019,5005,1,1200.00),(49,1019,5003,1,999.99),(50,1019,5009,1,999.99),(51,1020,5002,1,499.99),(52,1020,5008,1,980.00),(53,1020,5007,1,2500.00),(54,1020,5013,1,499.99),(55,1020,5010,1,249.99),(56,1021,5007,2,5000.00),(57,1021,5008,2,1960.00),(58,1021,5005,1,1200.00),(59,1022,5010,3,749.97),(60,1022,5007,1,2500.00),(61,1022,5013,1,499.99),(62,1023,5008,1,980.00),(63,1024,5009,2,1999.98),(64,1024,5008,3,2940.00);
/*!40000 ALTER TABLE `OrderItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payment`
--

DROP TABLE IF EXISTS `Payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Payment` (
  `OrderID` int(11) NOT NULL,
  `PaymentMethod` varchar(255) NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `PaymentDate` date NOT NULL,
  `Status` varchar(255) NOT NULL,
  PRIMARY KEY (`OrderID`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`OrderID`) REFERENCES `Order` (`OrderID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payment`
--

LOCK TABLES `Payment` WRITE;
/*!40000 ALTER TABLE `Payment` DISABLE KEYS */;
INSERT INTO `Payment` VALUES (1001,'Credit Card',299.99,'2024-01-15','Completed'),(1002,'Invoice',89.99,'2024-01-16','Processing'),(1003,'Debit Card',3249.98,'2024-02-01','Completed'),(1004,'Credit Card',249.99,'2024-02-05','Completed'),(1008,'Invoice',7200.00,'2024-03-30','Processing'),(1009,'Invoice',7200.00,'2024-03-30','Processing'),(1010,'Credit Card',21299.97,'2024-03-30','Processing'),(1015,'Debit Card',4549.97,'2024-04-03','Processing'),(1016,'Invoice',2949.95,'2024-04-03','Processing'),(1017,'Invoice',5429.94,'2024-04-03','Processing'),(1018,'Invoice',5699.94,'2024-04-03','Processing'),(1019,'Invoice',3699.97,'2024-04-05','Processing'),(1020,'Credit Card',4729.97,'2024-04-07','Processing'),(1021,'Credit Card',8160.00,'2024-04-11','Processing'),(1022,'Credit Card',3749.96,'2024-04-12','Processing'),(1023,'Credit Card',980.00,'2024-04-12','Processing'),(1024,'Invoice',4939.98,'2024-04-14','Processing');
/*!40000 ALTER TABLE `Payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` text DEFAULT NULL,
  `Price` decimal(10,2) NOT NULL,
  `StockQuantity` int(11) NOT NULL,
  `BrandID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProductID`),
  KEY `idx_product_price` (`Price`),
  KEY `idx_product_stock_quantity` (`StockQuantity`),
  KEY `idx_product_brand_id` (`BrandID`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`BrandID`) REFERENCES `Brand` (`BrandID`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5014 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (5001,'Galaxy Flex','Flexible smartphone by Samsung.',799.99,30,1001),(5002,'Pixel Slate','High-resolution tablet by Google.',499.99,15,1002),(5003,'iPhone 13','Latest smartphone from Apple.',999.99,100,1003),(5004,'Xperia 5','Flagship smartphone from Sony.',899.99,50,1004),(5005,'Gram 14','Ultra-lightweight laptop from LG.',1200.00,40,1005),(5006,'XPS 15','High-performance laptop from Dell.',1500.00,60,1006),(5007,'Alpha 7','Mirrorless camera from Sony.',2500.00,20,1004),(5008,'Fridge X','Energy-efficient refrigerator from LG.',980.00,40,1005),(5009,'MacBook Air','Lightweight laptop from Apple.',999.99,75,1003),(5010,'AirPods Pro','Wireless earbuds from Apple.',249.99,150,1003),(5011,'PlayStation 5','Latest gaming console from Sony.',499.99,100,1004),(5012,'Laptop Charger','Universal laptop charger.',49.99,200,NULL),(5013,'Galaxy Tablet','Tablet by Samsung.',499.99,0,1001);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductCategory`
--

DROP TABLE IF EXISTS `ProductCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductCategory` (
  `ProductID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  PRIMARY KEY (`ProductID`,`CategoryID`),
  KEY `CategoryID` (`CategoryID`),
  CONSTRAINT `productcategory_ibfk_1` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON UPDATE CASCADE,
  CONSTRAINT `productcategory_ibfk_2` FOREIGN KEY (`CategoryID`) REFERENCES `Category` (`CategoryID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductCategory`
--

LOCK TABLES `ProductCategory` WRITE;
/*!40000 ALTER TABLE `ProductCategory` DISABLE KEYS */;
INSERT INTO `ProductCategory` VALUES (5001,2001),(5002,2002),(5003,2001),(5004,2001),(5005,2003),(5006,2003),(5007,2004),(5008,2005),(5009,2003),(5010,2006),(5011,2006),(5012,2006),(5013,2002);
/*!40000 ALTER TABLE `ProductCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserCredentials`
--

DROP TABLE IF EXISTS `UserCredentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserCredentials` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) NOT NULL,
  `Password` char(60) NOT NULL,
  `Email` varchar(255) NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserCredentials`
--

LOCK TABLES `UserCredentials` WRITE;
/*!40000 ALTER TABLE `UserCredentials` DISABLE KEYS */;
INSERT INTO `UserCredentials` VALUES (1,'jdoe','pass123!','johndoe@example.com'),(2,'asmith','pass456!','annsmith@example.com'),(3,'tbell','pass789!','tinabell@example.com'),(4,'fchopin','pass101!','fredchopin@example.com'),(5,'Postman Pat','$2b$12$Uivi7YZ2PYBRC/anL1vnWenZY0tgsQJHqd2Djqky1iJrvT6LDCvMO','postman@pat.com'),(6,'Tele Tubbies','$2b$12$gIQZr9ruucoZz7/t0NIjdu2jRNMZYOgo0/RgkFAtXtWYmef9etCtC','Tele@tubbies.com'),(7,'tdoge','$2b$12$LQynHq.WwALRTkdTLxVmDe1e9BkB7jQOYJrsLo3N/QYHkIoxOkQ26','t@doge.com'),(8,'gskare','$2b$12$oAWvlS9yWRDZbl316jV3nOeiQndJHxlx.Cj/E9unO/hdLkBn40hVm','gskar@example.com'),(9,'ltest','$2b$12$nPJuhD0K9n.rrR1FXsZyWeYnBjvpeb5xWPpwP54aKu8gjQu2B4Gx.','ltest@mail.com'),(10,'tturing','$2b$12$yyCMg3zW8cWB1QpcB3lu6eTkiYCg8b6WbefwKoF2KH2QXMqPV4/lu','tturing@example.com'),(11,'ygard','$2b$12$X6kdkSFREq6tZ9k/O9WZFeY5dCFVSAFMMruijWzolwzy.6OF9GFWe','ygard@example.com'),(12,'Frode','$2b$12$i20PH787JQF6GmEzQczb.OCko3C47ONezWVnxGU2ap91e8rVubJYC','frode@ntnu.no'),(13,'rpenwood','$2b$12$m0A3I8mBOz0tN6ZEB7pJLejaFZ50tI8Ugx1gpgdeSsLvoXfNYH03G','rogerpenwood@example.com');
/*!40000 ALTER TABLE `UserCredentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserDetails`
--

DROP TABLE IF EXISTS `UserDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserDetails` (
  `UserID` int(11) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `Address` text NOT NULL,
  PRIMARY KEY (`UserID`),
  CONSTRAINT `userdetails_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `UserCredentials` (`UserID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserDetails`
--

LOCK TABLES `UserDetails` WRITE;
/*!40000 ALTER TABLE `UserDetails` DISABLE KEYS */;
INSERT INTO `UserDetails` VALUES (1,'John','Doe','123 Maple St, Anytown'),(2,'Ann','Smith','456 Oak St, Othertown'),(3,'Tina','Bell','789 Birch St, Thirdtown'),(4,'Fred','Chopin','1010 Cherry Blvd, Fourthtown'),(5,'Postman','Pat','Cat Street 222, Anywhere'),(6,'Tele','Tubbies','TV Street 2, NRK'),(7,'Tarzan','Doge','Island Boulevard 2, Jungle City'),(8,'Guri','Skare','Fjellvegen 2, Otta'),(9,'Last','Test','Testing street 1, Home'),(10,'Testing','Turing','England street 2, Computer Lab'),(11,'Ylva','Gard','Gard 3, Trosjø'),(12,'Frode','Øvrebø','Gjøvik'),(13,'Roger','Penwood','England Street 2, \nYorkhood 7755');
/*!40000 ALTER TABLE `UserDetails` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-19  0:46:45
