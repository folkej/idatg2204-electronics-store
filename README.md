# IDATG2204 - Electronics Store

This is intended so that the person grading this assignment may test the Electronics Store application. 

I have Dockerized the application with sample data in the MySQL-database. It should be quite simple to get it up and running. For it to work, the computer must have downloaded Git, Docker and Docker Compose.

- **Git**: [Installation Guide](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- **Docker**: [Installation Guide](https://docs.docker.com/get-docker/)
- **Docker Compose**: [Installation Guide](https://docs.docker.com/compose/install/)


Here are usage instructions in the form of a step-by-step guide:

## First step
Open a terminal or command prompt and clone the repository: 
```bash
git clone https://gitlab.stud.idi.ntnu.no/folkej/idatg2204-electronics-store.git
cd idatg2204-electronics-store
```

## Second step
Still in the folder where the docker-compose.yml is located, build and start all the necessary Docker containers: 
```bash
docker-compose up --build
```

This make take some time downloading the necessary packages and building the container. 

## Third step
To test the application, open a web browser and go to `http://localhost:3000`. This will access the frontend of the application. 

If one wishes to test the backend, this is on `http://localhost:5002`. 

To log in, one might use the username "tturing" and the password "123456". One can also register a new user. 

Of particular interest is to see that users created, products added to cart and orders made, are indeed inserted correctly into the database. To do this, one can run `docker ps` and look for the container-id of the mysql docker container. This id is then used to replace the <mysql-container-id> in the following command: 

```bash
docker exec -it <mysql-container-id> mysql -u root -p
```

(The docker container will be running in the terminal window after having done `docker-compose up --build`, so to run `docker ps` and then navigate to the MySQL interface, it should be done in a separate terminal window.)

This leads to an interactive session of the MySQL database. To enter the correct database, run ``USE `Electronics-Store`;``. From here one can show tables through `SHOW TABLES;` and also run SQL-queries, like `SELECT * FROM Users;`, to confirm the functionality of the application. 

## Fourth step
To stop the running container, run:
```bash
docker-compose down
```